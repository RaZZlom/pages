# Guix

## Что такое Guix?

GNU Guix ("Guix" произносится "гикс") - это утилита для управления пакетами и дистрибутив системы GNU. Guix
позволяет непривилегированным пользователям устанавливать, обновлять и удалять
программные пакеты, откатываться до предыдущих наборов пакетов, собирать пакеты
из исходников и обеспечивает создание и поддержку программного окружения в целом.

Вы можете установить GNU Guix поверх существующей системы GNU/Linux, и она
дополнит функции системы новой утилитой, не внося помехи. Или можно использовать
отдельную операционную систему — систему Guix.

## Установка Guix в обычную систему

Ребята из GNU рекомендуют устанавливать Guix с помощью скрипта, который они
написали.

Для этого выполняем следующие шаги:

Импортируем публичный ключ:

```sh
wget 'https://sv.gnu.org/people/viewgpg.php?user_id=15145' -qO - | sudo -i gpg --import -
```

Переходим в директорию /tmp:

```sh
cd /tmp
```

Скачиваем скрипт:

```sh
wget https://git.savannah.gnu.org/cgit/guix.git/plain/etc/guix-install.sh
```

Делаем скрипт выполняемым:

```sh
chmod +x guix-install.sh
```

Запускаем скрипт:

```sh
sudo ./guix-install.sh
```

На вопрос
`Permit downloading pre-built package binaries from the project's build farm?(yes/no)`
отвечаем `yes`.

В `/.bash_profile` добавляем следующие строки:

```sh
GUIX_PROFILE="/home/razzlom/.config/guix/current"
. "$GUIX_PROFILE/etc/profile"

GUIX_LOCPATH="$HOME/.guix-profile/lib/locale"
```

## Использование Guix

Первым делом выполняем:

```sh
guix pull
```

Это обновит наш guix до актуальной версии репозитория.

Затем, как было предложено раньше устанавливаем наш первый пакет:

```sh
guix install glibc-utf8-locales
```

## Ссылки

- [Руководство по GNU Guix](https://guix.gnu.org/manual/ru/html_node/)
